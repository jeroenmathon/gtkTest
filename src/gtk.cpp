//
// Created by jeroen on 9/16/15.
//

#include "gtk.h"

gtkInterface::gtkInterface()
{

}

gtkInterface::~gtkInterface()
{

}

void gtkInterface::btn_onclick()
{
    label.show();
}

int gtkInterface::run()
{
    window.set_title("Cuppy Cakies");
    window.set_default_size(500, 200);

    label.set_text("YAY :D");
    button.add_label("Press meh");

    box.add(button);
    box.add(label);
    window.add(box);

    button.show();
    box.show();
    button.signal_clicked().connect(sigc::mem_fun(this, &gtkInterface::btn_onclick));

    return 0;
}