#include "gtk.h"
#include <gtkmm.h>

int main(int argc, char *argv[])
{
    Glib::RefPtr<Gtk::Application> app =
            Gtk::Application::create(argc, argv, "Cupppy Cakies");

    gtkInterface gInt;
    gInt.run();
    return app->run(gInt.window);
}
