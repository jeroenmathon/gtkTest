//
// Created by jeroen on 9/16/15.
//

#ifndef GTK_TEST_GTK_H
#define GTK_TEST_GTK_H
#include <gtkmm.h>
#include <iostream>

class gtkInterface
{
public:
    // Constructors and Destructors
    gtkInterface();
    ~gtkInterface();

    // Public routines
    int run();

    Gtk::Window window;
private:
    // Private routines
    void btn_onclick();

    // Private variables

    Gtk::Box box;
    Gtk::Label label;
    Gtk::Button button;
};
#endif //GTK_TEST_GTK_H
